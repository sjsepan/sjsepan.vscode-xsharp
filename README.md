# XSharp (X#) for VS Code

## About

    This Visual Studio Code extension supports XSharp.Net (X#) with some syntax highlighting and code snippets. It is a (nearly) direct fork of 'xsharp-lang' in the 'vscode-xsharp-syntax' repository by Infominds Ag.

<https://github.com/InfomindsAg/vscode-xsharp-syntax/tree/main>

    However, that instance did not provide any snippets, so I decide to make one.
    This extension is based on that extension's current syntax (v1.0.3) which is for the current XLang (2.16).

<https://github.com/X-Sharp/XSharpDev>

## Features

- Syntax highlighting
    (e.g. `FOR`, `WHILE`, `SWITCH...CASE`, `IF...ELSEIF...ELSE`, `TRY...CATCH`, `REPEAT ... UNTIL`)
- Code snippets
    (e.g. `FOR`, `WHILE`, `SWITCH...CASE`, `IF...ELSEIF...ELSE`, `TRY...CATCH`, `REPEAT ... UNTIL`)

## Installation

### From repository

Download the .vsix file and choose 'Install from VSIX...' from the app.

## Changelog

See the [changelog](https://gitlab.com/sjsepan/sjsepan.vscode-xsharp/blob/HEAD/CHANGELOG.md) for details.

## Issues

-

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/15/2025
