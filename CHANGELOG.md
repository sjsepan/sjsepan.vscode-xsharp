# Changelog

## [0.0.7]

- canonical layout reorg

## [0.0.6]

- rev for publish testing on OVSX

## [0.0.5]

- rev for publish testing on OVSX

## [0.0.4]

- fix manual install instructions in readme

## [0.0.3]

- fix repo url in pkg

## [0.0.2]

- fix changelog
- rename publisher
- add more values to manifest
- fix content types xml
- fix pkg filename
- fix version scheme

## [0.0.1]

- Initial release with syntax highlighting & code snippets
